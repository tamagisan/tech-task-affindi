import type { Database, Post, User, Comment, Photo, Album } from './database';
import { faker } from '@faker-js/faker';
import { writeFileSync } from 'fs';
import { default as path } from 'path';

const database_file = process.env.DB_FILE
                      || path.join(__dirname, '../data/db.json');

const USERS_NUM = 5;
const POSTS_NUM = 10;
const COMMENTS_NUM = 4;
const PHOTOS_NUM = 12;
const ALBUMS_NUM = 5;

const database: Database = {
  users: [],
  posts: [],
  comments: [],
  photos: [],
  albums: [],
};

for (let id = 0; id < USERS_NUM; id++)
  database.users.push(gen_user(id));

for (let id = 0; id < POSTS_NUM; id++)
  database.posts.push(gen_post(id, USERS_NUM));

for (let id = 0; id < COMMENTS_NUM; id++)
  database.comments.push(gen_comment(id, USERS_NUM, POSTS_NUM));

for (let id = 0; id < PHOTOS_NUM; id++)
  database.photos.push(gen_photo(id, USERS_NUM, ALBUMS_NUM));

for (let id = 0; id < ALBUMS_NUM; id++)
  database.albums.push(gen_album(id, USERS_NUM));

function gen_user(id: number) {
  return {
    email: faker.internet.email(),
    // TODO: encode password
    password: faker.internet.password(10),
    id,
    geo: {
      lat: faker.address.latitude(),
      lon: faker.address.longitude(),
    },
  } as User;
}

function gen_post(id: number, users_num: number) {
  return {
    id,
    title: faker.random.words(3),
    text: faker.random.words(20),
    userId: Math.floor(Math.random() * users_num),
  } as Post;
}

function gen_comment(id: number, users_num: number, posts_num: number) {
  return {
    id,
    text: faker.random.words(15),
    postId: Math.floor(Math.random() * posts_num),
    userId: Math.floor(Math.random() * users_num),
  } as Comment;
}

function gen_photo(id: number, users_num: number, albums_num: number) {
  return {
    id,
    filename: faker.image.imageUrl(),
    date_added: faker.date.past().toISOString(),
    albumId: Math.floor(Math.random() * albums_num),
    userId: Math.floor(Math.random() * users_num),
  } as Photo;
}

function gen_album(id: number, users_num: number) {
  return {
    id,
    name: faker.random.words(3),
    userId: Math.floor(Math.random() * users_num),
  } as Album;
}

const pretty_json = JSON.stringify(database, null, 2);
writeFileSync(database_file, pretty_json);
