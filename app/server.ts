import type { Application } from 'express';
import { default as jsonServer } from 'json-server';
import { default as auth } from 'json-server-auth';

const rules = auth.rewriter({
  'posts': 664,
  'posts/:id': 644,
  'photos': 664,
  'photos/:id': 644,
  'comments': 664,
  'albums': 664,
  'albums/:id': 664,
});

const createServer = (source: string | object) => {
  const router = jsonServer.router(source);
  const server = jsonServer.create() as Application & {db: typeof router.db;};
  server.db = router.db;

  server.use(jsonServer.defaults());
  server.use(rules);
  server.use(auth);
  server.use(router);

  return server;
};

export { createServer };
