export interface Database {
  users: User[];
  posts: Post[];
  comments: Comment[];
  photos: Photo[];
  albums: Album[];
}

export interface User {
  email: string;
  password: string;
  id: number;
  geo: {
    lat: string;
    lon: string;
  }
}

export interface Post {
  id: number;
  title: string;
  text: string;
  userId: number;
}

export interface Comment {
  id: number;
  text: string;
  postId: number;
  userId: number;
}

export interface Photo {
  id: number;
  filename: string;
  date_added: string;
  albumId: number;
  userId: number;
}

export interface Album {
  id: number;
  name: string;
  userId: number;
}
