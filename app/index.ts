import { createServer } from './server';
import { default as path } from 'path';

const port = Number(process.env.PORT || 3000);
const database_file = process.env.DB_FILE
                      || path.join(__dirname, '../data/db.json');

const app = createServer(database_file);
app.listen(port);
