import { describe, test, expect, beforeEach } from 'vitest';
import { default as request } from 'supertest';
import { createServer } from '../app/server';
import { seed } from './seed';
import { default as clone } from 'clone';
import { performance } from 'perf_hooks';

describe.concurrent('/photos route', () => {
  beforeEach(context => {
    context.SEED_DB = Object.freeze(clone(seed));

    context.app_db = clone(seed);
    context.app = createServer(context.app_db);
    context.rq = request.agent(context.app);
  });

  test('GET /photos response time under 10s', async ({ rq }) => {
    const startTime = performance.now();
    await rq.get('/photos').expect(200);
    expect(performance.now() - startTime).toBeLessThan(10_000);
  });
});
