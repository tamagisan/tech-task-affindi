import type { Database } from '../app/database';
import { default as seed_db_json } from '../data/db.json';

const seed = seed_db_json as Database;
export { seed };
