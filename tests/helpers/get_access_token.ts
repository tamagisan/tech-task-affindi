import type { User } from '../../app/database';
import { default as jwt } from 'jsonwebtoken';
import { JWT_SECRET_KEY, JWT_EXPIRES_IN } from 'json-server-auth/dist/constants';

const get_access_token = (user: User | undefined) => {
  if (!user) throw new Error('No such user'); // TODO: I don't like this

  const payload = { email: user.email };
  const options = { expiresIn: JWT_EXPIRES_IN, subject: user.id.toString() };

  return jwt.sign(payload, JWT_SECRET_KEY, options);
};

export { get_access_token };
