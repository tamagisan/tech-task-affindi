import type { User } from '../app/database';
import { describe, test, expect, beforeEach } from 'vitest';
import { default as request } from 'supertest';
import { createServer } from '../app/server';
import { seed } from './seed';
import { default as clone } from 'clone';
import { faker } from '@faker-js/faker';
import { omit } from 'lodash';
import { get_access_token } from './helpers/get_access_token';

describe.concurrent('users', () => {
  beforeEach(context => {
    context.SEED_DB = Object.freeze(clone(seed));

    context.app_db = clone(seed);
    context.app = createServer(context.app_db);
    context.rq = request.agent(context.app);
  });

  test('GET /users gets all users', async ({ rq }) => {
    const res = await rq.get('/users')
        .expect('Content-Type', /json/)
        .expect(200);

    const users = res.body as User[];

    expect(users.length).toBeGreaterThanOrEqual(5);
    const user = users[4];

    const latitude = Number(user.geo.lat);
    expect(latitude).toBeGreaterThanOrEqual(-90);
    expect(latitude).toBeLessThanOrEqual(90);

    const longitude = Number(user.geo.lon);
    expect(longitude).toBeGreaterThanOrEqual(-180);
    expect(longitude).toBeLessThanOrEqual(180);

    // TODO: do we want to have users in specific area?
  });

  test('POST /register registers new user', async ({ rq }) => {
    const new_user = {
      email: faker.internet.email(),
      password: faker.internet.password(10),
      geo: {
        lat: faker.address.latitude(),
        lon: faker.address.longitude(),
      },
    };

    const res = await rq.post('/register')
        .send(new_user)
        .expect(201);

    const body = res.body as { accessToken: string, user: User };

    // TODO: ugly
    expect(body.user).toEqual({ ...omit(new_user, ['password']), id: expect.any(Number) as number });

    expect(body.accessToken).toBeDefined();
    expect(body.accessToken).toBeTypeOf('string');
    expect(body.accessToken).toBe(get_access_token(body.user));
  });
});
