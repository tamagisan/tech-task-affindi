import type { Database } from '../app/database';
import type { Application } from 'express';
import type { SuperAgentTest } from 'supertest';

declare module 'vitest' {
  export interface TestContext {
    SEED_DB: Database,
    app_db: Database,
    app: Application,
    rq: SuperAgentTest,
  }
}
