import { describe, test, expect, beforeEach } from 'vitest';
import { default as request } from 'supertest';
import { createServer } from '../app/server';
import { seed } from './seed';
import { default as clone } from 'clone';

describe.concurrent('/albums route', () => {
  beforeEach(context => {
    context.SEED_DB = Object.freeze(clone(seed));

    context.app_db = clone(seed);
    context.app = createServer(context.app_db);
    context.rq = request.agent(context.app);
  });

  test('GET /albums/3 verify content length', async ({ rq }) => {
    const res = await rq.get('/albums/3')
        .expect(200);

    interface ResponseHeaders { 'content-length': string }
    const headers = res.headers as ResponseHeaders;

    expect(headers['content-length']).toBeDefined();
    expect(headers['content-length']).toBeTypeOf('string');

    const content_length = Number(headers['content-length']);
    expect(content_length).toEqual(res.text.length);
  });
});
