import { describe, test, expect, beforeEach } from 'vitest';
import { default as request } from 'supertest';
import { createServer } from '../app/server';
import { seed } from './seed';
import { default as clone } from 'clone';
import { get_access_token } from './helpers/get_access_token';

describe.concurrent('/posts route', () => {
  beforeEach(context => {
    context.SEED_DB = Object.freeze(clone(seed));

    context.app_db = clone(seed);
    context.app = createServer(context.app_db);
    context.rq = request.agent(context.app);
  });

  test('GET /posts gets all posts', async ({ SEED_DB, app_db, rq }) => {
    const res = await rq.get('/posts')
        .expect('Content-Type', /json/)
        .expect(200);

    expect(res.body).toEqual(SEED_DB.posts);
    expect(app_db).toEqual(SEED_DB);
  });

  test('GET /posts/:id gets post', async ({ SEED_DB, app_db, rq }) => {
    const post = SEED_DB.posts[0];

    const res = await rq.get(`/posts/${post.id}`)
        .expect('Content-Type', /json/)
        .expect(200);

    expect(res.body).toEqual(post);
    expect(app_db).toEqual(SEED_DB);
  });

  test('POST /posts creates new post', async ({ SEED_DB, app_db, rq }) => {
    const user = SEED_DB.users[0];
    const token = get_access_token(user);

    const new_post = {
      title: "Ford... you're turning into a penguin. Stop it.",
      text: "This must be Thursday,' said Arthur to himself, sinking low over his beer. 'I never could get the hang of Thursdays.",
      userId: user.id,
    };

    const res = await rq.post('/posts')
        .set('Authorization', `Bearer ${token}`)
        .send(new_post)
        .expect('Content-Type', /json/)
        .expect(201);

    expect(res.body).toEqual({ ...new_post, id: expect.any(Number) as number });

    const last_post = app_db.posts.at(-1);
    expect(res.body).toEqual(last_post);
    expect(app_db.posts).toHaveLength(SEED_DB.posts.length + 1);
  });

  test('POST /posts fails if unauthorized', async ({ SEED_DB, app_db, rq }) => {
    const new_post = {
      title: 'sneaky post',
      text: 'no one will ever know mwahahaha',
    };

    const res = await rq.post('/posts')
        .send(new_post)
        // TODO: BUG: returns string, claims json
        .expect('Content-Type', /json/)
        .expect(401);

    expect(res.body).toBe('Missing authorization header');
    expect(app_db).toEqual(SEED_DB);
  });

  test('PUT /posts/:id updates existing post', async ({ SEED_DB, app_db, rq }) => {
    const post = SEED_DB.posts[0];
    const user = SEED_DB.users.find(u => u.id === post.userId);
    const token = get_access_token(user);

    const updated_post = {
      title: 'PANIC',
      text: 'We demand rigidly defined areas of doubt and uncertainty!',
      userId: post.userId,
    };

    const res = await rq.put(`/posts/${post.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send(updated_post)
        .expect('Content-Type', /json/)
        .expect(200);

    expect(res.body).toEqual({ ...updated_post, id: post.id });

    const app_post = app_db.posts.find(p => p.id === post.id);
    expect(res.body).toEqual(app_post);
  });

  // TODO: PUT /posts/:id is idempotent

  test('PUT /posts/:id fails if unauthorized', async ({ SEED_DB, app_db, rq }) => {
    const post = SEED_DB.posts[0];
    const new_post = {
      title: 'sneaky post update',
      text: 'no one will ever know mwahahaha',
    };

    const res = await rq.put(`/posts/${post.id}`)
        .send(new_post)
        // TODO: BUG: returns string, claims json
        .expect('Content-Type', /json/)
        .expect(401);

    expect(res.body).toBe('Missing authorization header');
    expect(app_db).toEqual(SEED_DB);
  });

  test('PUT /posts/:id fails if post does not exist', async ({ SEED_DB, app_db, rq }) => {
    const user = SEED_DB.users[0];
    const token = get_access_token(user);

    const new_post = {
      title: 'Is there any tea on this spaceship?',
      text: "I think you ought to know I'm feeling very depressed.",
      userId: user.id,
    };

    const max_post_id = Math.max(...SEED_DB.posts.map(p => p.id));
    const new_post_id = max_post_id + 1;
    const res = await rq.put(`/posts/${new_post_id}`)
        .set('Authorization', `Bearer ${token}`)
        .send(new_post)
        .expect('Content-Type', /json/)
        .expect(404);

    expect(res.body).toEqual({});
    expect(app_db).toEqual(SEED_DB);
  });

  test('PATCH /posts/:id updates existing post', async ({ SEED_DB, app_db, rq }) => {
    const post = SEED_DB.posts[0];
    const user = SEED_DB.users.find(u => u.id === post.userId);
    const token = get_access_token(user);

    const updated_fields = {
      title: 'The Ultimate Answer to Life, The Universe and Everything is...42!',
    };

    const res = await rq.patch(`/posts/${post.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send(updated_fields)
        .expect('Content-Type', /json/)
        .expect(200);

    expect(res.body).toEqual({ ...post, ...updated_fields });

    const app_post = app_db.posts.find(p => p.id === post.id);
    expect(res.body).toEqual(app_post);
  });

  // TODO: PATCH /posts/:id fails if unauthorized

  test('DELETE /posts/:id deletes existing post', async ({ SEED_DB, app_db, rq }) => {
    const post = SEED_DB.posts[0];
    const user = SEED_DB.users.find(u => u.id === post.userId);
    const token = get_access_token(user);

    const res = await rq.delete(`/posts/${post.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send()
        .expect('Content-Type', /json/)
        .expect(200);

    expect(res.body).toEqual({});
    expect(app_db.posts).toHaveLength(SEED_DB.posts.length - 1);
    // TODO: check that other posts were not deleted
  });

  // TODO: DELETE /posts/:id fails if post does not exist
  // TODO: DELETE /posts/:id fails if unauthorized
});
