import type { Comment } from '../app/database';
import { describe, test, expect, beforeEach } from 'vitest';
import { default as request } from 'supertest';
import { createServer } from '../app/server';
import { seed } from './seed';
import { default as clone } from 'clone';

describe.concurrent('/comments route', () => {
  beforeEach(context => {
    context.SEED_DB = Object.freeze(clone(seed));

    context.app_db = clone(seed);
    context.app = createServer(context.app_db);
    context.rq = request.agent(context.app);
  });

  test('GET /comments?_sort=postId&_order=desc sort by postId desc', async ({ rq }) => {
    const res = await rq.get('/comments?_sort=postId&_order=desc')
        .expect(200);

    const comments = res.body as Comment[];
    expect(comments.length).toBeGreaterThan(0);

    const post_ids = comments.map(c => c.postId);
    expect(post_ids).toEqual(post_ids.sort().reverse());

    // TODO: check that data is intact
  });
});
