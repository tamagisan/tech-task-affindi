# Tech task Affindi

API testing for [JSON-Server](https://github.com/typicode/json-server) + [JSON-Server-Auth](https://github.com/jeremyben/json-server-auth) app using data generation from [@faker-js/faker](https://github.com/faker-js/faker)

## run tests
 - `npm ci`
 - `npm run generate_db`
 - `npm t`
---
## start app(json server) on production
 - `npm ci --omit=dev`
 - `npm run seed_and_start`

or get latest docker image
 - `docker run -p 3000:3000 registry.gitlab.com/tamagisan/tech-task-affindi:latest`
