FROM node:18.3-alpine3.16

RUN mkdir /home/node/src
WORKDIR /home/node/src

COPY package*.json ./
RUN npm ci --omit=dev

COPY . .

EXPOSE 3000
ENTRYPOINT /usr/local/bin/npm run seed_and_start
